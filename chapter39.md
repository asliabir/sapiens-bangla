# দশ কিস্তি দুই

---

মানচিত্র আঁকার অভ্যাস আমাদের অনেক আগে থেকেই ছিল। সমস্যা হল, ঐ মানচিত্রগুলো ছিল অসম্পূর্ণ। আমরা, আফ্রো-এশিয়ার লোকজন তখন আমেরিকা সম্পর্কে জানতাম না। আমেরিকার আদি অধিবাসীরাও আমাদের সম্পর্কে জানতো না। কাজেই, আফ্রো-এশিয়ার মানচিত্র এঁকেই আমরা একে গোটা প্‌থিবীর মানচিত্র বলে চালিয়ে দিতাম।

আমাদের এই আইডিলোজিতে প্রথম আঘাতটা হানে ইউরোপীয়রা। তারা মানচিত্রের স্থানে স্থানে ফাঁকা রেখে একে আঁকতে শুরু করে। ইউরোপীয়ানদের চিন্তাধারায় যে বড়সড় পরিবর্তন আসছে, এটা তার প্রথম লক্ষণ। এশিয়ানরা যেখানে 'সব জেনে বসে আছি ভাব' নিয়ে লিটার‍্যালী বসে ছিল, সেখানে নিজেদের অজ্ঞানতাকে ওরা খোলা বাজারে স্বীকার করা শুরু করে। আর সেই অজানা ভূমির খোঁজে পাল তুলে বেরিয়ে পড়ে।

কলম্বাস নিজেও অবশ্য প্‌থিবীর পূর্ণাঙ্গ মানচিত্রে ঈমানধারীদের একজন ছিলেন। উনি হিসেব কষে দেখেছিলেন, স্পেন থেকে সোজা পশ্চিমে যাত্রা করলে ৭ হাজার কিলোমিটার পর তিনি জাপান পৌঁছে যাবেন। তার এই হিসাব ঠিক মিললো না। ৭ হাজার নয়, ২০ হাজার কিলোমিটার যাত্রার পর তার জাহাজের এক নাবিক অদূরে স্থলভাগের চিহ্ন দেখে "মাটি, মাটি" বলে চেঁচিয়ে ওঠে। এই স্থলভাগকেই আজ আমরা বাহামা বলে জানি।

স্পেন আর জাপানের মাঝখানে আর কিছু যে থাকতে পারে---কলম্বাস এতে বিশ্বাস করতেন না। তার বদ্ধমূল ধারণা হয়েছিল, তিনি এশিয়ার পূর্ব প্রান্তের কোন দ্বীপে এসে পৌঁছেছেন। তিনি এই নতুন মানুষদের নাম দিলেন ইন্ডিয়ান। তিনি যে একটা সম্পূর্ণ নতুন মহাদেশ আবিষ্কার করে ফেলেছেন---এই ব্যাপারটা তিনি নিতেই পারলেন না। কাজেকর্মে আধুনিক আর সাহসী হলেও মনে আর মননে কলম্বাস ছিলে পুরোপুরি মধ্যযুগের মানুষ। বাইবেলের উপর তার ছিল গভীর আস্থা। একটা বিরাট মহাদেশ থাকবে আর তার কথা বাইবেলে থাকবে না---এমনটা হতে পারে না। এই বিরাট ভুল নিয়েই তিনি সারা জীবন কাটিয়ে দেন।

এদিক থেকে প্রথম আধুনিক মানুষ ছিলেন আমেরিগো ভেসপুচি। কলম্বাসের কয়েক বছর পর ইতালির এই ভদ্রলোক আমেরিকায় সমুদ্রাত্রা করনে। সময়টা ছিল ১৪৯৯-১৫০৪। এই সময়ের মধ্যে তিনি দুটো বইও লিখে ফেলেন। এখানেই তিনি প্রথম দাবি করেন, কলম্বাস যেখানে আসছিলেন, ওটা আসলে এশিয়ার পূর্ব উপকূলের কোন দ্বীপ না। ওটা নতুন একটা মহাদেশের অংশ। যে মহাদেশ সম্পর্কে আমরা এখনো জানি না।

তার কথাবার্তায় কনভিন্সড হয়ে মার্টিন ওয়ালসেমুলার নামে সেকালের এক বিখ্যাত মানচিত্র নির্মাতা এই নতুন মহাদেশকে মানচিত্রে ঢুকিয়ে দেন। এখন এই নতুন মহাদেশের তো একটা নাম দিতে হবে। ওয়ালসেমুলারের ধারণা ছিল, আমেরিগো-ই এই নয়া মহাদেশের আবিষ্কারক। আমেরিগোর নাম অনুসারে তিনি এর নাম দেন আমেরিকা। তার এই ম্যাপখানা ব্যাপক জনপ্রিয়তা পায়। আর প্‌থিবীর বুকে আমেরিকা নামখানি-ই খোদাই হয়ে যায়।

কলম্বাসের নামে আমেরিকায় বেশ কিছু শহরের নাম আছে। দক্ষিণ আমেরিকায় একটা দেশের নামও আছে। কিন্তু ঐ গোঁয়ার্তুমিটা না করলে হয়তো আজ প্‌থিবীর দু দুটো মহাদেশের নাম আমেরিকা না হয় কলম্বিয়া হত। ইতিহাসের অল্প কয়টা 'পোয়েটিক জাস্টিস' এর মধ্যে এটা একটা। কলম্বাস তার অজ্ঞানতাকে স্বীকার করতে চাননি। আর আমেরিগো ভেসপুচির এই স্বীকারটুকু করার সাহস ছিল বলে নতুন পৃথিবীর নাম তার নামেই হল।

সায়েন্টিফিক রেভোল্যুশন এগজ্যাক্টলি কবে শুরু হয়, তার যদি কোন ল্যান্ডমার্ক চিহিত করতে হয়, তবে ইউরোপীয়ানদের এই আমেরিকা আবিষ্কার-ই সেই ল্যান্ডমার্ক। এর মধ্য দিয়ে মানচিত্র নির্মাতারাই কেবল মানচিত্রের ফাঁকা জায়গাগুলো ভরাট করা শুরু করলেন না, তাদের সাথে এগিয়ে এলেন জ্ঞানের সব শাখার লোকেরা। উদ্ভিদ ও প্রাণিবিদ, পদার্থ ও রসায়নবিদ, তাত্ত্বিক ও ধার্মিক---সবাই। সবাই পাগলের মত যার যার ফিল্ডের ফাঁকা জায়গাগুলো ভরাট করা শুরু করলেন।

ইউরোপীয় ছাড়া আর এক দলের সামান্য সম্ভাবনা ছিল নতুন প্‌থিবীর মালিক হবার। তারা হল চাইনিজ। কলম্বাসের যাত্রার বেশ আগে ১৪৩৩ সালে মিং রাজারা এক বিরাট লটবহর পাঠান ভারত মহাদেশ এক্সপ্লোর করতে। ৩০০ জাহাজ ওয়ালা সেই বহর শ্রীলংকা, ভারত, পারস্য উপসাগর ঘুরে আফ্রিকার পূর্বভাগে ছুঁয়ে যায়।

কলম্বাসের সাথে এই অভিযানের পার্থক্য ছিল---এরা একদম নতুন, অপরিচিত কোন ভূমির সন্ধান পায়নি। যে দেশগুলো ছুঁয়ে গেছে, তাদেরকেও কলোনাইজ করার চেষ্টা করেনি। আর সবচেয়ে বড় কথা, এই দুঃসাহসী অভিযান ব্যাপারগুলো কেন যেন চাইনিজ কালচারের সাথে খুব যায় না। তাদের ট্রাডিশনাল কালচারে গতির চেয়ে স্থিতিকেই বেশি গুরুত্ব দেয়া হয়েছে। এ্যাদ্ভেঞ্চার নয়, স্ট্যাবিলিটি তাদের মূল মোক্ষ। বেইজিং এ তাই ক্ষমতার পালাবদলের পর এইসব অভিযানে ফান্ডিং-ও গুটিয়ে আনা হয়। চাইনিজদের নতুন প্‌থিবী জয়ের স্বপ্নও সেইখানেই থমকে যায়।

ইউরোপীয়ানদের এই পাগলামির আমরা অনেক সময় ক্রেডিট দিতে চাই না। ইউরোপীয় কলোনিয়ালিজমের আমরা সমালোচনা করতেই পারি, সেই সাথে ওদের সাহসের প্রশংসাও করতে হবে। সেকালের প\[রস্পেক্টিভে এটা কিন্তু ছিল চূড়ান্ত মাত্রার দুঃসাহসিক যাত্রা। আজ আমাদের জন্য অন্য কোন গ্রহে বসতি স্থাপন করা যতোটা কঠিন, ইউরোপীয়দের অভিযানগুলোও ঠিক ততোটাই কঠিন আর অনিশ্চিত ছিল।

সেসময় যে মানুষ অন্য দেশ দখল করতে যাত্রা করতো না, তা না। কিন্তু সেটা ছিল নিজেদের আশেপাশের চল্লিশ দেশের মধ্যে সীমাবদ্ধ। খুব দূরে কোথাও যাত্রা করে সাম্রাজ্য স্থাপনের কথা এট লিস্ট কেউ ভাবতো না। তাও অজানা আর দীর্ঘ সমুদ্রপথে। আলেকজান্ডার বহুদূর থেকে এসে ভারবর্ষে আক্রমণ করসেন। কিন্তু এখানে সাম্রাজ্য স্থাপন করেননি। বা থেকে যাননি। থেকে গেছে মোগলরা। যারা এক অর্থে আমাদের প্রতিবেশীই ছিল।

আর একটা উদাহরণ দিই। রোমানরা এত্রুরিয়া জয় করসে ৩০০-৩৫০ খ্রিস্টপূর্বাব্দে। এত্রুরিয়া রক্ষা করার জন্য তারা জয় করসে পো ভ্যালী \(২০০ খ্রিস্টপূর্বাব্দে\)। পো ভ্যালী সিকিউরিটি নিশ্চিত করার জন্য জয় করসে জয় করসে প্রোভেন্স \(১২০ খ্রিস্টপূর্বাব্দে\), প্রোভেস্নকে রক্ষা করার জন্য গল \(আ জকের ফ্রান্স, ৫০ খ্রিস্টপূর্বাব্দে\) আর গলকে রক্ষা করার জনন্য ব্রিটেনের দখল বুজে নিসে ৫০ খ্রিস্টাব্দে। লন্ডন থেকে রোমে যাইতে তাদের ৪০০ বছর লেগে গেছে। ৩৫০ খ্রিস্টাব্দে রোমানরা ভুলেও লন্ডন জয়ের স্বপ্ন দেখতো না। ইউরোপীয়রা সেটাই দেখসে। আগের সমস্ত ধ্যান-ধারণা ভেঙে সম্পূর্ণ অজানার ইদ্দেশ্যে যাত্রা করসে। আর সফলও হইসে।

আজ যে আমরা সবাই এক প্‌থিবীর বাসিন্দা, এক ধরণী মাতার সন্তান---এই বোধটা কিন্তু ইউরোপীয়দের এই অভিযানগুলোই আমাদের মধ্যে তৈরি করসে। এর মাশুলও আমাদের দিতে হয়েছে। একটা নয়, দুটো নয়, চার চারটে মহাদেশ ইউরোপীয়দের নামে লিখে দিয়ে।

