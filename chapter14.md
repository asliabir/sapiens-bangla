# চার কিস্তি এক

---

আমাদের একান্ত ব্যক্তিগত স্বপ্নগুলোও আসলে 'ইমাজিনড অর্ডার'-ই ঠিক করে দেয়।

পপুলার কালচার আমাদের বলে---Follow your heart. হ্‌দয়ের দাবি শোনো। এখন এই হ্‌দয়ের দাবি তো শূন্য থেকে ভেসে ওঠা কোন চাহিদা নয়। হাজার বছরের সভ্যতায় এই দাবিগুলো তৈরি হয়েছে। ঊনিশ শতকের রোমান্টিসিজম আর বিশ শতকের কনজ্যুমারিজম সেই পালে আরো হাওয়া লাগিয়েছে। আমরা নিজেদের যতোই বিদ্রোহী, বিপ্লবী বা হাওয়া বিরোধী দাবি করি না কেন, দিন শেষে আমরা সেই হাওয়ার সাথেই চলি।

আপনার হয়তো ছুটিছাটায় খুব বেড়ানোর শখ। এই শখ কিন্তু আমাদের পূর্বপুরুষদের ছিল না। এক আলফা শিম্পাঞ্জী গায়ের জোর খাটিয়ে আরেক দল শিম্পাঞ্জীর বাসায় দাওয়াত খেতে যেতো না। আমাদের মিশরীয় দাদুরা অবসরে কক্সবাজার না গিয়ে বানাতেন পিরামিড। আর মমি। এই কক্সবাজার যাওয়ার কালচারটা শুরু করে মূলত রোমানরা। রোমান সাম্রাজ্যের শেষ দিকে অভিজাতরা তাদের সামার কাটানোর জন্য বেছে নিত মিশরকে। রোমানদের সেই কালচারই আজকের বিরাট পর্যটন শিল্পে রূপ নিয়েছে।

রোমান্টিসিজম আমাদের বলে, জীবনটাকে যতো পারো, উপভোগ করে নাও। যতো বেশি পারো, অভিজ্ঞতা অর্জন করে নাও। আমাদের তাই সব রকমের বই পড়ে দেখতে হবে, সব রকমের মুভি চেখে দেখতে হবে, সব রকমের খাবারের স্বাদ নিতে হবে, সব রকম রিলেশনশিপের মজা নিতে হবে, আর যতো যতো দর্শনীয় স্থান আছে---সবগুলো দেখে মরতে হবে।

কেন ভাই? কেন আমাকে সব কিছুই করতে হবে? গিভ মি এ্যা ব্রেক।

কিন্তু রোমান্টিসিজম আপনাকে ব্রেক দিলেও কনজ্যুমারিজম আপনাকে ব্রেক দিবে না। সে সারাক্ষণ আপনার চোখের সামনে এই সবের বিজ্ঞাপন নিয়ে হাজির হতে থাকবে। Ten things you must do before you get 30, ten places you must visit before you die, ten girls/boys you must date before you marry---এই সবই হচ্ছে পুঁজিবাদের টোপ। পত্রিকা ম্যাগাজিনের গণ্ডি ছাড়িয়ে সেই টোপ চলে এসেছে আপনার মুঠোফোনে। দিনরাত এগুলো দেখে আপনিও এক সময় নিজের বাকেট লিস্ট টৈরি করা শুরু করেন। কনজ্যুমারিজম আপনাকে যেই ছাঁচের মানুষ বানাতে চাচ্ছে, আপনি নিজের অজান্তে সেই ছাঁচের মানুষে পরিণত হচ্ছেন। যদিও মনে মনে ভাবছেন---আমার হ্‌দয় আমাকে এটা বলছে। সত্যটা হচ্ছে এই যে, হ্‌দয় আপনাকে বলছে না। বলছে পুঁজিবাদ। আমি আপনি তাতে সাড়া দিচ্ছি কেবল।

কনজ্যুমারিজমের কাজই হচ্ছে প্রোডাক্ট বিরক্রি করা। আর অভিজ্ঞতা হচ্ছে সবচেয়ে বড় প্রোডাক্ট। আপনার কানের কাছে এসে দিনরাত বিড়বিড় করা হবে---নায়াগ্রা ফলস না দেখলে আপনার জীবন ব্‌থা। কিংবা "প্‌থিবীতে দুই রকমের মানুষ আছে--এক, যারা নায়াগ্রা ফলস দেখসে। আর এক, যারা তা দেখেনি।" আপনি নিশ্চয়ই দুই নম্বর দলে পড়ে থাকতে চাইবেন না। প্রিয় মানুষদের কম সময় দিয়ে হলেও আপনি এক্সট্রা খেটে সেই অভিজ্ঞতাটা কিনতে চাইবেন।

আপনি হয়তো এই মধ্যবিত্তের দলে পড়েন না। আপনি পড়েন বিত্তবানদের দলে। আপনার সাথে আপনার বউয়ের রিলেশনশিপ ক্রাইসিস চলছে। তো এই ক্রাইসিস মেটানোর জন্য আপনি তাকে লন্ডন বা প্যারিস নিয়ে গেলেন। অথচ রাত করে বাড়িতে না ফিরে বউকে সময় দিলেই হয়তো এই ক্রাইসিস মেটানো যেত। কিন্তু পুঁজিবাদের চাকা তো তাইলে বন্ধ হয়ে যাবে। পুঁজিবাদ চাইবে, আপনাদের মধ্যে বারংবার ঝগড়া হোক। আর আপনারা বেশি বেশি করে হিল্লিদিল্লি করুন।

ওয়েল, এই কাজটাই ফারাওরা করতো তাদের বউয়ের জন্য তার মন পছন্দ সমাধি বানিয়ে। বউ নিয়ে তারা ব্যাবিলন ঘুরতে যেতো না। মোগল সম্রাটেরা করতেন তাজমহল বানিয়ে। আমাদের তাজমহল বানানোর সামর্থ্য নেই। কাজেই, আমরা বেশি বেশি করে এক্সপেরিয়েন্স নেবার চেষ্টা করি। এই এক্সপেরিয়েন্সগুলোই আমাদের তাজমহল। সমস্ত সভ্যতাই এভাবে তাজমহল বানিয়ে গেছে। তাজমহলের রূপটা কেবল বদলেছে যুগে যুগে।

হ্‌দয়ের দাবি নয়, সময়ের দাবীই তাই আমরা শুনে চলেছি বারবার। সাজেকের ইতিহাস হয়তো আপনার জানা। কীভাবে সেখানকার মানুষদের উচ্ছেদ করে সাজেক বানানো হয়েছে---আপনি তার খুঁটিনাটি জানেন। কাজেই, সাজেকে যেতে আপনার মন ঠিক সায় দেয় না। এদিকে বন্ধুবান্ধবের তোলা ছবি দেখে আপনার ঘুম পাড়ানো ইচ্ছেটা আবার জেগে ওঠে। আর এটা তো জানা কথাই যে, সোশ্যাল মিডিয়া হচ্ছে কনজ্যুমারিজমের সবচেয়ে বড় চারণক্ষেত্র। আপনি গাড়ি, বাড়ি, ডিএসএলার---যাই কিনেন না কেনো, আপনাকে একটা ম্য্যণ্ডাটরি ছবি দিতেই হবে। মানুষ একটা কিছু অর্জন করে যতোটা না সুখ পায়, তার চেয়ে অনেক বেশি সুখ পায় অন্যের মধ্যে হাহাকার তৈরি করে। দিন শেষে আপনিও তাই ব্যাকপ্যাক নিয়ে সাজেকের উদ্দেশে রওনা দেন। জিত হয় কনজ্যুমারিজমের।

নিজের কথাই বলি। শাদা ইউরোপীয়রা এককালে আমেরিকার আদি অধিবাসীদের মোটামুটি নিশ্চিহ্ন করে তাদের ইমাজিনড অর্ডার তৈরি করেছে এখানে। আমরা যে বিশ্ববিদ্যালয়ে পড়ি, সেখানটিতেই হয়তো কোন আদিবাসী ফ্যামিলির সুখী, সুন্দর সংসার ছিল। তাদের রক্তের উপর দাঁড়িয়ে আমরা জ্ঞানচর্চা করে চলেছি। এই জ্ঞানটুকু থাকার পরও আমরা কিন্তু এখানে আসি। এসে অন্যদের উপদেশ দিই, ফলো ইয়োর হার্ট। যদিও আমরাও সময়ের দাবিতেই এখানে এসেছি।

সবাই যে কনজ্যুমারিজমের দাস---তা অবশ্যই নয়। এক আধজন পাগল সব জায়গাতেই থাকে, যারা কিনা হ্‌দয়ের দাবি শোনে। এদের মধ্য থেকেই কেউ নিউটন হয়, আর কেউ হয় কৈলাশ সত্যার্থী।

